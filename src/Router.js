import Component from "./components/Component.js";

export default class Router{
    static titleElement;
    static contentElement;
    static routes;


    static navigates(currentPath){
        Router.routes.forEach(({path, page, title}) => {
            if (currentPath == path){
                Router.titleElement.innerHTML = new Component('h1', null, title).render();
                Router.contentElement.innerHTML = page.render();
            }
        });
        
    }
}
