import data from './data.js';
import Component from './components/Component.js';
import Img from './components/Img.js';
import PizzaThumbnail from './components/PizzaThumbnail.js';
import PizzaList from './pages/PizzaList.js';
import Router from './Router.js';

/*const title = new Component( 'h1', null, ['La carte'] );
document.querySelector('.pageTitle').innerHTML = title.render();
*/

const pizzaList = new PizzaList([]);

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');
Router.routes = [{ path: '/', page: pizzaList, title: 'La carte' }];

//Router.navigates('/'); // affiche une page vide
pizzaList.pizzas = data;
Router.navigates('/');


/*const pizzaList = new PizzaList(data);
Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');
Router.routes = [
	{path: '/', page: pizzaList, title: 'La carte'}
];
Router.navigates('/');
*/
/*const pizza = data[1];
const pizzaThumbnail = new PizzaThumbnail(pizza);
document.querySelector( '.pageContent' ).innerHTML = pizzaThumbnail.render();
*/

//document.querySelector( '.pageContent' ).innerHTML = pizzaList.render();

/*const pizzaList = new PizzaList(data);
document.querySelector( '.pageContent' ).innerHTML = pizzaList.render();
*/

