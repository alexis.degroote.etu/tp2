import Component from "../components/Component.js";
import PizzaThumbnail from "../components/PizzaThumbnail.js";

export default class PizzaList extends Component{ 
    #pizzas;
    set pizzas(value){
        this.#pizzas = value;
        this.children = this.#pizzas.map(element => new PizzaThumbnail(element));
    }
    

    constructor(data){
        super('section', {name: 'class', value: "pizzaList"}, data.map(element => new PizzaThumbnail(element)));
        this.#pizzas = data;
    }

}