export default class Component {
    tagName;
	attribute;
    children;

    constructor(tagName, attribute, children){
        this.tagName = tagName;
		this.attribute = attribute;
        this.children = children;
    }

    render(){
        if (this.children && this.attribute == null) {
            return `<${this.tagName}> ${this.renderChildren(this.children)} </${this.tagName}>`;
        } else if (this.children && this.attribute){
            return `<${this.tagName} ${this.attribute.name}="${this.attribute.value}"> ${this.renderChildren(this.children)} </${this.tagName}>`;
        }else if (this.attribute && this.children == null){
            return `<${this.tagName} ${this.attribute.name}="${this.attribute.value}" />`;
        }

        return `<${this.tagName} />`;;
    }

    renderChildren(children) {
        if (children instanceof Array)
            return children.map(this.renderChildren).join('');

        if (children instanceof Component)
            return children.render();
        
        return children;
    }

}
